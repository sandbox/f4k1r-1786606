<?php

/**
 * @file
 * Default theme implementation for displaying search results.
 *
 * This template collects each invocation of theme_search_result(). This and
 * the child template are dependent to one another sharing the markup for
 * definition lists.
 *
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 *
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 *
 *
 * @see template_preprocess_search_results()
 */
?>
<?php
global $pager_page_array, $pager_total_items;
$count = variable_get('apachesolr_rows', $_SESSION['resultsperpage']);
$start = $pager_page_array[0] * $count + 1;
print t('<div class="search-count">A total of %total results found.</div>',
  array(
    '%total' => $pager_total_items[0]
  )
);
?>
<?php if ($search_results): ?>
  <ol class="search-results <?php print $module; ?>-results">
    <?php print $search_results; ?>
  </ol>
 <div class="search-pager"> <?php print $pager; ?>
<?php print drupal_render(drupal_get_form('as_pager_solr_form'));?></div>
<?php else : ?>
  <h2><?php print t('Your search yielded no results');?></h2>
  <?php print search_help('search#noresults', drupal_help_arg()); ?>
<?php endif; ?>
